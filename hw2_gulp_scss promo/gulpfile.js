const gulp = require("gulp");
const concat = require("gulp-concat");
const htmlmin = require('gulp-htmlmin');
const terser = require('gulp-terser');
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
const clean = require('gulp-clean');
const imagemin = require('gulp-imagemin');
var browserSync = require('browser-sync').create();



import gulp from 'gulp';
import concat from "gulp-concat";
import imagemin from 'imagemin';
import cleanCSS from 'gulp-clean-css';
import gulpSass from "gulp-sass";
import nodeSass from "sass";
const sass = gulpSass(nodeSass);
import autoprefixer  from 'gulp-autoprefixer';
import clean from 'gulp-clean';
import uglify from "gulp-uglify";
const pipeline = require('readable-stream').pipeline;
import purify from "gulp-purify-css";
import BS from "browser-sync";
const browserSync = BS.create();

const { src, dest, task, watch, series } = gulp;
// clean
const cleanDist = () => src('dist/*', { allowEmpty: true }).pipe(clean());
// scss in css
const convertScss = () => src('src/scss/**/*')
    .pipe(sass())
    .pipe(dest('dist/styles'));
//brwser-async
const startWatching = () => {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    watch('src/scss/**/*', "src/js/**/*").on('all', series(cleanDist, convertScss, browserSync.reload));
}
//concat and minify JS
const concatJS = () => gulp.src('./js/*.js')
.pipe(concat('script.min.js'))
.pipeline(
    gulp.src('js/*.js'),
    uglify(),
    gulp.dest('dist')
)
.pipe(gulp.dest('./dist/'));
//concat and cleanCSS and autoprefixer CSS purifyCSS
const concatCSS = () => gulp.src('./scss/*.css')
.pipe(concat('style.min.css'))
.pipe(purify(['./js/*.js', './*.html']))
.pipe(cleanCSS({compatibility: 'ie8'}))
.pipe(autoprefixer({
    cascade: false
}))
.pipe(gulp.dest('./dist/'));
//imagemin
const minifyImages = () => gulp.src('img/*')
.pipe(imagemin())
.pipe(gulp.dest('dist/img'));
// 



task('dev', startWatching);
task('build', series(cleanDist, convertScss, minifyImages, concatJS, concatCSS));



//