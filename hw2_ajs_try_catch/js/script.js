
// Теоретический вопрос
// Приведите пару примеров, когда уместно использовать в коде конструкцию try...catch.
// Всегда, когда функция или метод может прийти к ошибочной ситуации.
// При валидации входящих данных.

// Задание
// Дан массив books.
// Выведите этот массив на экран в виде списка (тег ul - список должен быть сгенерирован с помощью Javascript).
// На странице должен находиться div с id="root", куда и нужно будет положить этот список (похожая задача была дана в модуле basic).
// Перед выводом обьекта на странице, нужно проверить его на корректность (в объекте должны содержаться все три свойства - author, name, price). Если какого-то из этих свойств нету, в консоли должна высветиться ошибка с указанием - какого свойства нету в обьекте.
// Те элементы массива, которые являются некорректными по условиям предыдущего пункта, не должны появиться на странице.

const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];


function createList (array) {
    let div = document.createElement("div");
    div.id = "root";
    let ul = document.createElement("ul");
  array.map(function(item, i, arr) {

    if(Object.keys(item).length == 3){
      let li = document.createElement("li");
      li.append(`${item.author} `);
      li.append(`${item.name} `);
      li.append(item.price);
      ul.append(li)
      div.append(ul);
      document.body.append(div);
    } else{
      console.error("Short_properties_list");
    }
    });
  
}

createList(books);
