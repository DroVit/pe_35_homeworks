"use strict"

// Переключатель табов  в блоке Our services

let tabItems = document.querySelectorAll(".content-services-link");
let contentItem = document.querySelectorAll(".content-services-items");

tabItems.forEach(function(element) {
   element.addEventListener("click", function() {
        let currentDataTab = this.getAttribute("data-tab");
        let content = document.querySelector('.content-services-items[data-tab="'+currentDataTab+'"]')
        
        let activeTab = document.querySelector(".content-services-link.services-link-active");
        let activeContent = document.querySelector(".content-services-items.active-services");
        
        activeTab.classList.remove("services-link-active"); 
        element.classList.add("services-link-active"); 
        
        activeContent.classList.remove("active-services"); 
        content.classList.add("active-services"); 
   });
});


//Фильтр в блоке Our Amazing Work

const imageFilter = document.querySelectorAll(".content-amazing-work-image-item");
let filterClick = document.querySelector(".content-filter-amazing-work");

filterClick.addEventListener("click", function(evt){

    if(evt.target.tagName !== "LI") {
        return false;
    }
    let filterClass = evt.target.closest("a").dataset["filter"];

    imageFilter.forEach(function(element){
        element.classList.remove("imageHidden")
        if(!element.classList.contains(filterClass) && filterClass !== "all"){
            element.classList.add("imageHidden");
}
    })
})

const activeLink = document.querySelectorAll(".content-amazing-work-link");
const activeLinkNow = document.querySelectorAll(".content-amazing-work-link-active");

    activeLink.forEach(function(element){
            element.addEventListener("click", function(){
                activeLink.forEach(function(element){
                    element.classList.remove("content-amazing-work-link-active");
                })
                    element.classList.add("content-amazing-work-link-active");
            })                   
    })

//Подгрузка картинок в блоке Our Amazing Work

const btn = document.querySelector(".button-load-more");

btn.addEventListener("click", function (){

    let imageBox = document.querySelectorAll(".content-amazing-work-image-item.load-image");
        imageBox.forEach(element => {
            element.classList.remove("load-image");
        });
    btn.remove(btn);

})

// Слайдер в блоке What People Say About theHam

const buttonLeft = document.querySelector(".slide-left");
const buttonRight = document.querySelector(".slide-right");
const userPhoto = document.querySelectorAll(".user-photo");
const userReview = document.querySelectorAll(".user-review");
const userHidePhoto = document.querySelectorAll(".photo-decrease-item")



    let userData = 0;
    userHidePhoto.forEach(function(elem){
        elem.addEventListener("click", function(evt){
            userHidePhoto.forEach(function(elems){
                elems.classList.remove("user-active");
            })
        evt.target.closest(".photo-decrease-item").classList.add("user-active");   
        userData = evt.target.closest(".photo-decrease-item").dataset["user"];
            userPhoto.forEach(function(el){
                el.classList.add("user-hidden");
                el.classList.add("user-animation");             
                if(el.dataset["user"] == userData){
                    el.classList.remove("user-hidden");
                    el.classList.add("user-animation");
                }
            })
            userReview.forEach(function(e){
                e.classList.add("user-hidden");             
                if(e.dataset["user"] == userData){
                    e.classList.remove("user-hidden");
                    e.classList.add("user-animation-text");
                }
            })
        })      
    })


const className = "user-active";

let index = 2;
let indexRight;

buttonRight.addEventListener("click", () => {

    userHidePhoto.forEach(function(element, index){ 
        if (element.classList.contains("user-active") === true && index !== "2"){
                    userHidePhoto.forEach(function(element, index){
                        if (element.classList.contains("user-active") === true){
                            element.classList.remove(className);
                            return indexRight = index;}                                         
                    }) 
        }
        if (element.classList.contains("user-active") === true && index == "2") {
            indexRight = 2;}
    })

    userHidePhoto[index]?.classList.remove(className);
    index = (indexRight + 1) % userHidePhoto.length;
    userHidePhoto[index].classList.add(className);
    let userPosition = userHidePhoto[index].dataset["position"];
                                              
            userPhoto.forEach(function(el){
              el.classList.add("user-hidden");
              if(el.dataset["position"] == userPosition){
                  el.classList.remove("user-hidden");
                  el.classList.add("user-animation");
              }
            })
            userReview.forEach(function(e){
                e.classList.add("user-hidden");
                if(e.dataset["position"] == userPosition){
                    e.classList.remove("user-hidden");
                    e.classList.add("user-animation-text");
                }
            })
})


buttonLeft.addEventListener("click", () => {

    userHidePhoto.forEach(function(element, index){ 
       
        if (element.classList.contains("user-active") === true && index !== "2"){
                    userHidePhoto.forEach(function(element, index){
                        if (element.classList.contains("user-active") === true && index !== "0"){
                            element.classList.remove(className);
                            return indexRight = index;}   
                    }) 
        }
        if (index == "0") {
             return indexRight = 4;}
        if (element.classList.contains("user-active") === true && index == "2") {
            indexR = 2;}  
    })

  userHidePhoto[index]?.classList.remove(className);
  index = indexRight - 1;
  userHidePhoto[index].classList.add(className);
  let userPosition = userHidePhoto[index].dataset["position"];


  userPhoto.forEach(function(el){
    el.classList.add("user-hidden");
    if(el.dataset["position"] == userPosition){
        el.classList.remove("user-hidden");
        el.classList.add("user-animation");
    }
})
  userReview.forEach(function(e){
    e.classList.add("user-hidden");
    if(e.dataset["position"] == userPosition){
        e.classList.remove("user-hidden");
        e.classList.add("user-animation-text");
    }
})
});

// Masonry

let galleryBox = document.querySelector('.gallery-box');
var msnry = new Masonry( galleryBox, {
  itemSelector: ".gallery-image",
  columnWidth: 378,
  horizontalOrder: true,
  gutter: 10 
});



// Подгрузка картинок Masonry

var appendButton = document.querySelector('.button-gallery');
appendButton.addEventListener( 'click', function() {
  var elems = [];
  var fragment = document.createDocumentFragment();
    var elem = getItemElement();
    fragment.appendChild( elem );
    elems.push( elem );
  galleryBox.appendChild( fragment );
  msnry.appended( elems );
  appendButton.remove(appendButton);
});

function getItemElement() {
  var elem = document.createElement('div');
  elem.insertAdjacentHTML("beforeend", `<div class="gallery-image load-image-gallery"><img class="img-middle" src="img/gallery/gallery-01.jpg" alt="#"></div>
  <div class="gallery-image load-image-gallery"><img class="img-middle" src="img/gallery/gallery-02.jpg" alt="#"></div>
  <div class="gallery-image load-image-gallery"><img class="img-middle" src="img/gallery/gallery-03.jpg" alt="#"></div>
  <div class="gallery-image load-image-gallery"><img class="img-middle" src="img/gallery/gallery-04.jpg" alt="#"></div>
  <div class="gallery-image load-image-gallery"><img class="img-middle" src="img/gallery/gallery-05.jpg" alt="#"></div>
  <div class="gallery-image load-image-gallery"><img class="img-middle" src="img/gallery/gallery-06.jpg" alt="#"></div>`)
  return elem;
}