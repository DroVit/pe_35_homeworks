"use strict"

function createNewUser (){
    const firstName = prompt("What is your name?");
    const lastName = prompt("What is your lastname?");
    const birthday = prompt("When is your birthday?");

    let newUser = {
        getLogin: function () {
            return firstName[0].toLowerCase() + lastName.toLowerCase();
        },

        getAge: function () {   
            const dateNow = new Date();
            let yearUser = new Date(Date.parse(birthday.slice(6, )));
            return dateNow.getFullYear() - yearUser.getFullYear();
        },

        getPassword: function(){
            return firstName.charAt(0).toUpperCase() + lastName.toLowerCase() + birthday.slice(6, );
        },
    };

    Object.defineProperty(newUser, "firstName",
        {value: firstName,
        writable: false,
        configurable: true,
            get function () {
                return this.firstName;   
            },
            set function (newFirstName) {
                return this.firstName = newFirstName;
            },
        })

    Object.defineProperty(newUser, "lastName",
        {value: lastName,
        writable: false,
        configurable: true,
            get function () {
                return this.lastName;   
            },
            set function (newLastName) {
                return this.lastName = newLastName;
            },
        })

    Object.defineProperty(newUser, "birthday",
        {value: birthday,
        writable: false,
        configurable: true,
            get function () {
                return this.birthday;   
            },
            set function (newBirthday) {
                return this.birthday = newBirthday;
            },
        })


    newUser.setFirstName = function (setNewFirstName) {
        Object.defineProperty(newUser, "firstName",{
            value: setNewFirstName,
            writable: true,
            configurable:true,
            get function () {
                return this.firstName;
            },
            set function (setNewFirstName) {
                return this.firstName = setNewFirstName;
            }

        })
    }

    newUser.setLastName = function (setNewLastName) {
        Object.defineProperty(newUser, "lastName",{
            value: setNewLastName,
            writable: true,
            configurable:true,
            get function () {
                return this.lastName;
            },
            set function (setNewLastName) {
                return this.lastName = setNewLastName;
            }
        })
    };
    
        return newUser;
    };

const user = createNewUser();
    
console.log(user);

console.log(user.getLogin());

console.log(user.getAge());

console.log(user.getPassword());


// Теоретический вопрос

// Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования?

// Экранирование нужно для написание отдельных логических символов в строках к примеру если нам нужно записать в
// переменную со строкой кавычки, то нужно использовать другие кавычки. Если этого не сделать, будет синтаксическа ошибка (не закрытая кавычка).
// Если нужно использовать символы которые имеют особое значение, без применения этого собого начения, как текст. 

