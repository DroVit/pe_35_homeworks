"use strict"

function filterBy(arr, dataType){
    const result = arr.filter(item => typeof item !== dataType)
        return result;
}
console.log(filterBy([23, "rt", null, "65", 9, "dd", true, 54, undefined, 3n, {}, function(){}, Symbol("id")], "object"));


//Альтернативные варианты

// /////////////////////////////////////////////
// const filterBy = (arr, dataType) => arr.filter(item => typeof item !== dataType)

// console.log(filterBy([23, "rt", null, "65", 9, "dd", true, 54, undefined, 3n, {}, function(){}, Symbol("id")], "string"));
///////////////////////////////////////////////


//////////////////////////////////////////////
// function filterBy(array, dataType) {
//     return array.reduce((result, currentItem) => { if (typeof currentItem != dataType) {result.push(currentItem);} return result} , []); 
//    }
//    console.log(filterBy([23, "rt", null, "65", 9, "dd", true, 54, undefined, 3n, {}, function(){}, Symbol("id")], "number"));
///////////////////////////////////////////////



// Теоретический вопрос

// Опишите своими словами как работает цикл forEach?

// Перебирает елементы массива применяя к каждому написаную callback функцию.

