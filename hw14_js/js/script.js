"use strict"

$(document).ready(function(){
    $("#nav").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 2000);
    });
});

/////////////////////////////////////////////////////////////////////////

$(function() {
    $(window).scroll(function() {
    if($(this).scrollTop() > $(window).height()) {
        
    $('#toTop').fadeIn(); 
    } else { 
    $('#toTop').fadeOut(); 
    }
    });
    $('#toTop').click(function() {  
    $('body,html').animate({scrollTop:0},3000);  
    }); 
    });

    console.log(document.documentElement.clientHeight);


    // let pageYLabel = 0; //переменная будет хранить координаты прокрутки

    // $(window)
    // .load(function() {
    //  var pageY = $(window).scrollTop();
    // if (pageY > 200) {
    //  $("#toTop").show();
    // } //при загрузке страницы если прокрутка больше 200px - показывать кнопку вверх
    // })
    // .scroll(function(e){
    //  var pageY = $(window).scrollTop();
    //  var innerHeight = $(window).innerHeight();
    //  var docHeight = $(document).height();
    // if (pageY > innerHeight) {
    //         $("#toTop").show();
    // }else{$("#toTop").hide();}
   
    // if (pageY < innerHeight && pageYLabel >= innerHeight) {
    //         $("#down").show();
    // }else{$("#down").hide();}
    // }); //действия при прокрутке страницы




///////////////////////////////////////////////////////////////

    // $( document ).ready(function(){
        // $( ".toggle" ).click(function(){ // задаем функцию при нажатиии на элемент с классом toggle
        //   $( "div" ).toggle(); //  скрываем, или отображаем все элементы <div>
        // });
    //     $( "#slide-toggle" ).click(function(){ // задаем функцию при нажатиии на элемент с классом slide-toggle
    //       $( "#hot-news" ).slideToggle(); // плавно скрываем, или отображаем все элементы <div>
    //     });
    //   });


      $( "#slide-toggle" ).click(function(){ // задаем функцию при нажатиии на элемент с классом slide-toggle
	    $( "#hot-news" ).slideToggle( { // отображаем, или скрываем элементы <div> в документе
	      duration: 800, // продолжительность анимации
	      easing: "linear", // скорость анимации
	      complete: function(){ // callback
	        console.log("slideToggle completed");
	      },
	      queue: false // не ставим в очередь
	    });
	  });