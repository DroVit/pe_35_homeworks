"use strict"

let button = document.createElement("button");
let getButton = document.querySelector("button");
let logoBlock = document.querySelector(".logo-company-name");
let logo = document.querySelector(".company-name-link");

function buttonShow(){
    
    button.innerHTML = "Сменить тему";
    button.classList.add("button");
    logoBlock.append(button);
}

logo.addEventListener("mouseover", buttonShow);


function getRandom(min, max){
    return Math.ceil(Math.random() * (max - min) + min);
  }

  let contentItemHover = document.querySelectorAll(".content-item-hover");
  let contentItemText = document.querySelectorAll(".content-item-text");
  let mainContent = document.querySelector(".main-content");


function changeTheme(){

    if((localStorage.getItem("imageToChange") && localStorage.getItem("colorTheme")) !== null){

            localStorage.clear();

            mainContent.style.backgroundImage = "url(img/background.png)";
        
            contentItemHover.forEach(function(element){   
                element.style.backgroundColor = "#fc4a1a";
               })
            
               contentItemText.forEach(function(element){
                   element.style.color = "#fff";
               })   
               button.remove();
    }else{
   
            const colorItem = `rgb(${getRandom(0, 255)}, ${getRandom(0, 255)}, ${getRandom(0, 255)})`;
            
            localStorage.setItem("colorTheme", colorItem.toString());

            contentItemHover.forEach(function(element){   
                element.style.backgroundColor = colorItem;
            })

            contentItemText.forEach(function(element){
                element.style.color = colorItem;
            })

            mainContent.style.removeProperty("background-image");
            mainContent.style.backgroundImage = "url(img/backgroundChange.jpg)";
            
            localStorage.setItem("imageToChange","url(img/backgroundChange.jpg)");

            button.remove();
    }
}

button.addEventListener("click", changeTheme);


window.addEventListener("load", function(){

    mainContent.style.backgroundImage = localStorage.getItem("imageToChange");

    contentItemHover.forEach(function(element){   
        element.style.backgroundColor = localStorage.getItem("colorTheme");
       })

       contentItemText.forEach(function(element){
        element.style.color = localStorage.getItem("colorTheme");
    })
});








// Задание
// Реализовать возможность смены цветовой темы сайта пользователем. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// Технические требования:

// Взять любое готовое домашнее задание по HTML/CSS.
// Добавить на макете кнопку "Сменить тему".
// При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.) на ваше усмотрение. При повтором нажатии - возвращать все как было изначально - как будто для страницы доступны две цветовых темы.
// Выбранная тема должна сохраняться и после перезагрузки страницы


// Литература:

// LocalStorage на пальцах