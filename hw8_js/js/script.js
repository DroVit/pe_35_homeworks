"use strict"

window.onload = function(){

    const div = document.createElement("div");
    const input = document.createElement("input");
    const p = document.createElement("p");
    div.append(p);
    p.innerHTML = "Price";
    div.append(input);
    document.body.append(div);
    div.style.display = "flex";
    p.style.marginRight = "20px";
    p.style.marginTop = "0";
    p.style.marginBottom = "0";
    input.setAttribute("type", "number");
  
    const massege = document.createElement("p");

    function focusIn(evt){
        evt.target.style.border = "2px solid green"; 
        span.remove();
        button.remove();
        massege.remove();
    };
  
  function focusOut (evt){
        evt.target.style.removeProperty("border");
    if(input.value < 0){
        evt.target.style.border = "2px solid red"; 
        massege.innerHTML = "Please enter correct price";
        massege.style.marginLeft = "52px";
        div.after(massege); 
      };  
    if(input.value > 0){
        div.before(span);
        div.before(button);
        button.innerHTML = "&#11198";
        button.style.padding = "0";
        button.style.fontSize = "10px";
        span.innerHTML = "Текущая цена: " + `${input.value}`;
        span.style.marginLeft = "52px";
        input.style.color = "green";                 
      };
    };

    const span = document.createElement("span");
    const button = document.createElement("button");
    function remove(){
          span.remove();
          button.remove();
          input.value = "";
          input.style.removeProperty("color");
    };
    button.addEventListener("click", remove);
  
    input.addEventListener("focus", focusIn);     
    input.addEventListener("blur", focusOut);
  }

// Теоретический вопрос

// Опишите своими словами, как Вы понимаете, что такое обработчик событий?

// Функция, которая обрабатывает, или откликается на событие.