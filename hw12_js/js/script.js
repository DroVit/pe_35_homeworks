"use strict"

"use strict"

let buttonOn = document.querySelector(".button-start");

function start(){
    let imagesWrapper = document.querySelector(".images-wrapper");
    let buttonDiv = document.createElement("div");

    let images = document.querySelectorAll('.image-to-show');
    let currentImage = 0;
    let slideInterval = setInterval(nextSlide, 3000);
    
    function nextSlide() {
        images[currentImage].className = 'image-to-show';
        currentImage = (currentImage + 1) % images.length;
        images[currentImage].className = 'image-hidden';
    }
      
    function buttonCreate(){
        buttonDiv.classList.add("button-div","hidden");
        imagesWrapper.after(buttonDiv);
        
        let buttonStop = document.createElement("button");
        buttonStop.innerHTML = "Прекратить";
        buttonStop.classList = "button-stop";
        buttonDiv.append(buttonStop);
    
        let buttonGo = document.createElement("button");
        buttonGo.innerHTML = "Возобновить";
        buttonGo.classList = "button-go";
        buttonDiv.append(buttonGo);
    }
    buttonCreate();
    
    let buttonStopClick = document.querySelector(".button-stop");
    function buttonStopSlide(){
       clearInterval(slideInterval);  
    }
    buttonStopClick.addEventListener("click", buttonStopSlide);
    
    let buttonGoClick = document.querySelector(".button-go");
        function buttonGoSlide(){
            slideInterval = setInterval(nextSlide, 3000);      
    }
    buttonGoClick.addEventListener("click", buttonGoSlide);

    nextSlide();

    buttonOn.remove();
    imagesWrapper.classList.remove("hidden");
    buttonDiv.classList.remove("hidden");
    
}

buttonOn.addEventListener("click", start);





// #Теоретический вопрос

// Опишите своими словами разницу между функциями setTimeout() и setInterval().
// Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?
// Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?

// setTimeout - вызывает функцию один раз через интервал времени.
// setInterval - вызывает функцию многократно через определенный интервал времени.

// Вызов функции будет настолько быстро, насколько это возможно. Но планировщик будет вызывать функцию только после завершения выполнения текущего кода.
// Так вызов функции будет запланирован сразу после выполнения текущего кода.


// Функция clearInterval() отменяет многократные повторения действий, установленные вызовом функции setInterval(). 
// Если не отмнить будет непрерывно вызывать функцию через промежуток времени. Переполнит стек вызовов и выдасть ошибку.



// Задание
// Реализовать программу, показывающую циклично разные картинки. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// Технические требования:

// В папке banners лежит HTML код и папка с картинками.
// При запуске программы на экране должна отображаться первая картинка.
// Через 3 секунды вместо нее должна быть показана вторая картинка.
// Еще через 3 секунды - третья.
// Еще через 3 секунды - четвертая.
// После того, как покажутся все картинки - этот цикл должен начаться заново.
// При запуске программы где-то на экране должна появиться кнопка с надписью Прекратить.
// По нажатию на кнопку Прекратить цикл завершается, на экране остается показанной та картинка, которая была там при нажатии кнопки.
// Рядом с кнопкой Прекратить должна быть кнопка Возобновить показ, при нажатии которой цикл продолжается с той картинки, которая в данный момент показана на экране.
// Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.


// Необязательное задание продвинутой сложности:

// При запуске программы на экране должен быть таймер с секундами и миллисекундами, показывающий сколько осталось до показа следующей картинки.
// Делать скрытие картинки и показывание новой картинки постепенным (анимация fadeOut / fadeIn) в течение 0.5 секунды.