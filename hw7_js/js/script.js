"use strict"

function list (array, parentElement = "document.body"){
    let element = document.createElement(parentElement);
    let ol = document.createElement("ol");
 
   array.map(function(currentValue, index, array) {
                let li = document.createElement("li");
                li.innerHTML = array[index];
                ol.append(li);
           });          
    element.append(ol);
    document.body.append(element);   

// timer
    let time = 3;
    let span = document.createElement("span");
    function timer() {
        if(time<=0){
            element.remove();
            clearInterval(timer);
            span.remove();
            return;
        };
        span.innerHTML = "";
        span.innerHTML = time + " seconds";
        span.style.fontSize = "26px";
        span.style.color = "red";
        document.body.append(span);
        time--;
    };
    setInterval(timer, 1000);
};


list(["hello", "world", "Kiev", ["Glazgo", "Koln", "Bremen"], "Kharkiv", "Odessa", "Lviv"], "ul");




// Теоретический вопрос

// Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM) ?

// DOM – это объектная модель документа, которую браузер создаёт в памяти компьютера на основании HTML-кода.
// DOM – это набор связанных объектов, созданных браузером при чтении HTML-кода.

