"use strict"

function createNewUser (){
    const firstName = prompt("What is your name?");
    const lastName = prompt("What is your lastname?");

    let newUser = {
        getLogin: function () {
            return firstName[0].toLowerCase() + lastName.toLowerCase();
        },
    };

    Object.defineProperty(newUser, "firstName",
        {value: firstName,
        writable: false,
        configurable: true,
            get function () {
                return this.firstName;   
            },
            set function (newFirstName) {
                return this.firstName = newFirstName;
            },
        })

    Object.defineProperty(newUser, "lastName",
        {value: lastName,
        writable: false,
        configurable: true,
            get function () {
                return this.lastName;   
            },
            set function (newLastName) {
                return this.lastName = newLastName;
            },
        })

    newUser.setFirstName = function (setNewFirstName) {
        Object.defineProperty(newUser, "firstName",{
            value: setNewFirstName,
            writable: true,
            configurable:true,
            get function () {
                return this.firstName;
            },
            set function (setNewFirstName) {
                return this.firstName = setNewFirstName;
            }

        })
    }

    newUser.setLastName = function (setNewLastName) {
        Object.defineProperty(newUser, "lastName",{
            value: setNewLastName,
            writable: true,
            configurable:true,
            get function () {
                return this.lastName;
            },
            set function (setNewLastName) {
                return this.lastName = setNewLastName;
            }
        })
    };
    
        return newUser;
    };

const user = createNewUser();
    
console.log(user);

console.log(user.getLogin());

// Установка значения через сеттер

// let setNewFirstName = "Uasyu";
// user.setFirstName(setNewFirstName);
// console.log(user);

// user.firstName = "Uasya";
// console.log(user);




///Теоретический вопрос
// Опишите своими словами, что такое метод обьекта?

// Это действия которые можно выполнять с обьектом  посредством функций, которые хранятся как свойства в обьекте.