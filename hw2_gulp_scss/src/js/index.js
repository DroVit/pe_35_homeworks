"use strict"

const open = document.querySelector('.header__nav');
const cross = document.getElementById("closeBtn");
const burger = document.getElementById('burgerBtn');
console.log(burger);
console.log(open);

burger.addEventListener('click', () => {
    open.style.display = "block";
    burger.classList.add("button__wrapper-hidden");
    cross.classList.remove("button__wrapper-hidden");

});

cross.addEventListener('click', () => {
    open.style.display = "none";
    burger.classList.remove("button__wrapper-hidden");
    cross.classList.add("button__wrapper-hidden");
});