const gulp = require("gulp");
const concat = require("gulp-concat");

const sass = require('gulp-sass')(require('sass'));

const cleanCSS = require('gulp-clean-css');
const minify = require('gulp-minify');
const uglify = require('gulp-uglify');
const pipeline = require('readable-stream').pipeline;

const autoprefixer = require('gulp-autoprefixer');
const clean = require('gulp-clean');
const image = require('gulp-image');

const browserSync = require('browser-sync').create();

const purgecss = require('gulp-purgecss');
const cssmin = require('gulp-cssmin');
const rename = require('gulp-rename');

//////
// import gulp from 'gulp';

// import sass from "gulp-sass";

// import concat from "gulp-concat";
// import cleanCSS from "gulp-clean-css";
// import uglify from "gulp-uglify";
// import autoPrefixer from 'gulp-autoprefixer';
// import imagemin from 'gulp-imagemin';
// import clean from "gulp-clean";
// import gulpPurgecss from 'gulp-purgecss';
// import cssmin from "gulp-cssmin";
// import rename from "gulp-rename";

// import BS from "browser-sync";
// const browserSync = BS.create();
//////

const { src, dest, task, watch, series } = gulp;

// concat
const buildJs = () =>
    gulp.src('src/js/*.js')
      .pipe(concat('index.js'))
      .pipe(uglify())
      .pipe(rename({suffix: '.min'}))
      .pipe(gulp.dest('dist/js'));

//+sass in css + concat + autoprefixer +minify
  const buildStyles = () =>
    gulp.src('src/scss/*.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
    }))
    //   .pipe(purgecss({
    //     content: ['/*.html']
    // }))
      .pipe(concat('index.css'))
      .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
      .pipe(cleanCSS({compatibility: 'ie8'}))
      .pipe(gulp.dest('dist/css'));
    
    gulp.task("buildStyles", buildStyles);
//+clean+
    const cleanDist = () =>
         gulp.src('dist/*', {read: false})
            .pipe(clean());
    
    gulp.task("cleanDist", cleanDist);

//imagemin -
const minifyImages = () => 
	gulp.src('src/img/*')
		.pipe(image())
		.pipe(gulp.dest('dist/img'))

gulp.task("minifyImages", minifyImages);

//browser-sync
const startWatching = () => {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    watch('src/scss/**/*').on('all', series(buildStyles, browserSync.reload));
    watch('src/js/**/*').on('all', series(buildJs, browserSync.reload));
    watch('src/img/**/*').on('all', series(minifyImages, browserSync.reload));
};

gulp.task("dev", startWatching);
//

gulp.task("build", gulp.series(cleanDist, minifyImages, buildStyles, buildJs,));