// Теоретический вопрос
// Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript?

//Ответ
/*Каждый объект имеет внутреннюю ссылку на другой объект, называемый его прототипом. 
У объекта-прототипа также есть свой собственный прототип и так далее по цепочке.
Каждый объект содержит ссылку на свой объект-прототип.
При попытке получить доступ к какому-либо свойству объекта, свойство вначале ищется в самом объекте, затем в прототипе объекта, после чего в прототипе прототипа, и так далее. 
Поиск ведётся до тех пор, пока не найдено свойство с совпадающим именем или не достигнут конец цепочки прототипов.
*/
// Задание
// Реализовать класс Employee, в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). Сделайте так, чтобы эти свойства заполнялись при создании объекта.
// Создайте геттеры и сеттеры для этих свойств.
// Создайте класс Programmer, который будет наследоваться от класса Employee, и у которого будет свойство lang (список языков).
// Для класса Programmer перезапишите геттер для свойства salary. Пусть он возвращает свойство salary, умноженное на 3.
// Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.


class Employee {
    /**
     * 
     * @param {string} name - имя
     * @param {number} age - возвраст
     * @param {number} salary - зарплата
     */
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
// name
    get name (){
        return this._name;
    }

    set name (newName){
        this._name = newName;
    }
// age
    get age (){
        return this._age = age;
    }

    set age (newAge){
        this._age = newAge ;
    }
//  salary
    get salary (){
        return this._salary;
    }

    set salary (newSalary){
        this._salary = newSalary;
    }
}

/**
 * Создаем касс на основе класса Employee
 */

class Programmer extends Employee {
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this.lang = lang;
    }

//  salary
    get salary (){
        return this._salary * 3;
    }
}

/**
 * Создаем объекты на основе класса Programmer
 */

const Jhon = new Programmer("Jhon", 30, 2000, "ua");
console.log(Jhon);

const Roy = new Programmer("Roy", 28, 3000, "ru");
console.log(Roy);

const Bob = new Programmer("Bob", 33, 3500, "en");
console.log(Bob);
console.log(Bob.salary);
Bob.salary = 3000;
Bob.age = 50;
console.log(Bob);

