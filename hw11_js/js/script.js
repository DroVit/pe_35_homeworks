"use strict"

function buttonChangeColor (evt){
    let button = document.querySelectorAll(".btn");

button.forEach(element => {

    if(element.classList.contains("active")){
        element.classList.remove("active")
    }
    if(element.innerHTML == evt.key.toUpperCase() || element.innerHTML == evt.key){
        element.classList.toggle("active");
    } 
});   
};
    
document.addEventListener("keydown", buttonChangeColor);



// Теоретический вопрос

// Почему для работы с input не рекомендуется использовать события клавиатуры?

// Так как на современных устройствах есть и другие способы «ввести что-то». 
// Например, распознавание речи (это особенно актуально на мобильных устройствах) или Копировать/Вставить с помощью мыши.
// Поэтому, если мы хотим корректно отслеживать ввод в поле <input>, то одних клавиатурных событий недостаточно. 
// Существует специальное событие input, чтобы отслеживать любые изменения в поле <input>. 

