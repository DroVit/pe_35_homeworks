"use strict"

// Переключатель табов  в блоке Our services

let tabItems = document.querySelectorAll(".content-services-link");
let contentItem = document.querySelectorAll(".content-services-items");

tabItems.forEach(function(element) {
   element.addEventListener("click", function() {
        let currentDataTab = this.getAttribute("data-tab");
        let content = document.querySelector('.content-services-items[data-tab="'+currentDataTab+'"]')
        
        let activeTab = document.querySelector(".content-services-link.services-link-active");
        let activeContent = document.querySelector(".content-services-items.active-services");
        
        activeTab.classList.remove("services-link-active"); 
        element.classList.add("services-link-active"); 
        
        activeContent.classList.remove("active-services"); 
        content.classList.add("active-services"); 
   });
});


//Фильтр в блоке Our Amazing Work

const imageFilter = document.querySelectorAll(".content-amazing-work-image-item");
let filterClick = document.querySelector(".content-filter-amazing-work");

filterClick.addEventListener("click", function(evt){

    if(evt.target.tagName !== "LI") {
        return false;
    }
    let filterClass = evt.target.closest("a").dataset["filter"];

    imageFilter.forEach(function(element){
        element.classList.remove("imageHidden")
        if(!element.classList.contains(filterClass) && filterClass !== "all"){
            element.classList.add("imageHidden");
}
    })
})

const activeLink = document.querySelectorAll(".content-amazing-work-link");
const activeLinkNow = document.querySelectorAll(".content-amazing-work-link-active");

    activeLink.forEach(function(element){
            element.addEventListener("click", function(){
                activeLink.forEach(function(element){
                    element.classList.remove("content-amazing-work-link-active");
                })
                    element.classList.add("content-amazing-work-link-active");
            })                   
    })

//Подгрузка картинок в блоке Our Amazing Work

const btn = document.querySelector(".button-load-more");

btn.addEventListener("click", function (){

    let imageBox = document.querySelectorAll(".content-amazing-work-image-item.load-image");
        imageBox.forEach(element => {
            element.classList.remove("load-image");
        });
    btn.remove(btn);

})

// Слайдер в блоке What People Say About theHam

const buttonLeft = document.querySelector(".slide-left");
const buttonRight = document.querySelector(".slide-right");
const userPhoto = document.querySelectorAll(".user-photo");
const userReview = document.querySelectorAll(".user-review");
const userHidePhoto = document.querySelectorAll(".photo-decrease-item")

let userData = 0;
userHidePhoto.forEach(function(elem){
   
    elem.addEventListener("click", function(evt){
        userHidePhoto.forEach(function(elems){
            elems.classList.remove("user-active");
        })
    evt.target.closest(".photo-decrease-item").classList.add("user-active");
    userData = evt.target.closest(".photo-decrease-item").dataset["user"];
        userPhoto.forEach(function(el){
            el.classList.add("user-hidden");
            if(el.dataset["user"] == userData){
                el.classList.remove("user-hidden");
            }
        })
        userReview.forEach(function(e){
            e.classList.add("user-hidden");
            if(e.dataset["user"] == userData){
                e.classList.remove("user-hidden");
            }
        })
    })      
})

const className = "user-active";

let index = 2;
let indexR;

buttonRight.addEventListener("click", () => {

    userHidePhoto.forEach(function(element, index){ 
        if (element.classList.contains("user-active") === true && index !== "2"){
                    userHidePhoto.forEach(function(element, index){
                        if (element.classList.contains("user-active") === true){
                            console.log(index);
                            element.classList.remove(className);
                            return indexR = index;}                                         
                    }) 
        }
        if (element.classList.contains("user-active") === true && index == "2") {
            indexR = 2;}
    })

    userHidePhoto[index]?.classList.remove(className);
    index = (indexR + 1) % userHidePhoto.length;
    console.log(index);
    userHidePhoto[index].classList.add(className);
    let userPosition = userHidePhoto[index].dataset["position"];
                                              
            userPhoto.forEach(function(el){
              el.classList.add("user-hidden");
              if(el.dataset["position"] == userPosition){
                  el.classList.remove("user-hidden");
              }
            })
            userReview.forEach(function(e){
                e.classList.add("user-hidden");
                if(e.dataset["position"] == userPosition){
                    e.classList.remove("user-hidden");
                }
            })
})


buttonLeft.addEventListener("click", () => {

    userHidePhoto.forEach(function(element, index){ 
       
        if (element.classList.contains("user-active") === true && index !== "2"){
                    userHidePhoto.forEach(function(element, index){
                        if (element.classList.contains("user-active") === true && index !== "0"){
                            console.log(index);//
                            element.classList.remove(className);
                            return indexR = index;}   
                    }) 
        }
        if (index == "0") {
             return indexR = 4;}
        if (element.classList.contains("user-active") === true && index == "2") {
            indexR = 2;}  
    })

  userHidePhoto[index]?.classList.remove(className);
  index = indexR - 1;
  userHidePhoto[index].classList.add(className);
  let userPosition = userHidePhoto[index].dataset["position"];


  userPhoto.forEach(function(el){
    el.classList.add("user-hidden");
    if(el.dataset["position"] == userPosition){
        el.classList.remove("user-hidden");
    }
})
  userReview.forEach(function(e){
    e.classList.add("user-hidden");
    if(e.dataset["position"] == userPosition){
        e.classList.remove("user-hidden");
    }
})
});

////////////////
$( "#path" ).click(function(e) {
    $( userPhoto ).animate({/// гдето тут должны быть функция
        opacity: 0,
      }, 1000,
    );

    $( userReview ).animate({/// гдето тут должны быть функция
      opacity: 0,
    
    }, 1000,
  );
})

$( "#path" ).click(function(e) {
    $( userPhoto ).animate({/// гдето тут должны быть функция
        opacity: 1,
      }, 2000,
    );
    $( userReview ).animate({
      opacity: 1,
    
    }, 2000,
  );
})



///////////////////



// Masonry

const galleryBox = document.querySelector('.gallery-box');
var msnry = new Masonry( galleryBox, {
  // options
  itemSelector: ".gallery-image",
  columnWidth: 378,
  horizontalOrder: true,
  gutter: 10
  
});

// const container = document.querySelector('.gallery-box');
// const masnry;
// // Инициализация Масонри, после загрузки изображений
// imagesLoaded( container, function() {
// 	masnry = new Masonry( container );
// });






// Animate

// function makeEaseOut(timing) {
//     return function(timeFraction) {
//       return 1 - timing(1 - timeFraction);
//     }
//   }

//   function bounce(timeFraction) {
//     for (let a = 0, b = 1, result; 1; a += b, b /= 2) {
//       if (timeFraction >= (7 - 4 * a) / 11) {
//         return -Math.pow((11 - 6 * a - 11 * timeFraction) / 4, 2) + Math.pow(b, 2)
//       }
//     }
//   }

//   let bounceEaseOut = makeEaseOut(bounce);

//   brick.onclick = function() {
//     animate({
//       duration: 1000,
//       timing: bounceEaseOut,
//       draw: function(progress) {
//         // brick.style.bottom = progress * 100 + 'px';
//         brick.style.opacity = progress * 0.9;
//         brick.style.height = progress * 72 + 'px';
       

//       },

//     });
//   };