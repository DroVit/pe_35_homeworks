"use strict"

// Переключатель табов  в блоке Our services

let tabItems = document.querySelectorAll(".content-services-link");
let contentItem = document.querySelectorAll(".content-services-items");

tabItems.forEach(function(element) {
   element.addEventListener("click", function() {
        let currentDataTab = this.getAttribute("data-tab");
        let content = document.querySelector('.content-services-items[data-tab="'+currentDataTab+'"]')
        
        let activeTab = document.querySelector(".content-services-link.services-link-active");
        let activeContent = document.querySelector(".content-services-items.active-services");
        
        activeTab.classList.remove("services-link-active"); 
        element.classList.add("services-link-active"); 
        
        activeContent.classList.remove("active-services"); 
        content.classList.add("active-services"); 
   });
});


//Фильтр в блоке Our Amazing Work

const imageFilter = document.querySelectorAll(".content-amazing-work-image-item");
let filterClick = document.querySelector(".content-filter-amazing-work");

filterClick.addEventListener("click", function(evt){

    if(evt.target.tagName !== "LI") {
        return false;
    }
    let filterClass = evt.target.closest("a").dataset["filter"];

    imageFilter.forEach(function(element){
        element.classList.remove("imageHidden")
        if(!element.classList.contains(filterClass) && filterClass !== "all"){
            element.classList.add("imageHidden");
}
    })
})

const activeLink = document.querySelectorAll(".content-amazing-work-link");
const activeLinkNow = document.querySelectorAll(".content-amazing-work-link-active");

    activeLink.forEach(function(element){
            element.addEventListener("click", function(){
                activeLink.forEach(function(element){
                    element.classList.remove("content-amazing-work-link-active");
                })
                    element.classList.add("content-amazing-work-link-active");
            })                   
    })

//Подгрузка картинок в блоке Our Amazing Work

const btn = document.querySelector(".button-load-more");

btn.addEventListener("click", function (){

    let imageBox = document.querySelectorAll(".content-amazing-work-image-item.load-image");
        imageBox.forEach(element => {
            element.classList.remove("load-image");
        });
    btn.remove(btn);

})

// Слайдер в блоке What People Say About theHam

const buttonLeft = document.querySelector(".slide-left");
const buttonRight = document.querySelector(".slide-right");

const userPhoto = document.querySelectorAll(".user-photo");
const userReview = document.querySelectorAll(".user-review");

const userHidePhoto = document.querySelectorAll(".photo-decrease-item")
console.log(userHidePhoto );

function slide (evt){
    
    let slideStep;
//Удалили класс у фото юзера, скрыли его
    userPhoto.forEach(function(element, index){
        // ++slideStep
            if(element.classList.contains("user-hidden") === false){  
                element.classList.add("user-hidden"); 
                slideStep = index;
                console.log(slideStep); 
                }    
                        
    })

     
   
   
   
    
}
let res = 0;
    
userHidePhoto.forEach(function(element){
    
    element.addEventListener("click", function(evt){
        userHidePhoto.forEach(function(elem){
            elem.classList.remove("user-active");
        })
            if(element.classList.contains("user-active") === false){
                element.classList.add("user-active"); 
            }  
       
            
        let userData = evt.target.closest(".photo-decrease-item").dataset["user"];
              
        userPhoto.forEach(function(el){
            el.classList.add("user-hidden");
            el[userdata].remove("user-hidden");
        })
        
        
console.log(el[userdata]);

        // if(res>3){
        //     res = 0;
        // }else{
        // console.log(res);
        //  res++;}
    })
    
    
        
    });

    // buttonRight.addEventListener("click", slide);






// userPhoto.forEach(function(element){

//     element.addEventListener("click", function(){

//         if(element.classList.contains("user-hidden") === false){
//             element.classList.add("user-hidden");
//             userHidePhoto[index+1].classList.remove("user-active");  
//             }
            
//         userPhoto[slideStep+3].classList.remove("user-hidden");
//         userHidePhoto[3].classList.add("user-active");
//     })

// });








// Masonry

// const elem1 = document.querySelector('.gallery-box');
// var msnry1 = new Masonry( elem1, {
//   // options
//   itemSelector: '.gallery-image',
//   columnWidth: 346,
//   horizontalOrder: true
  
// });


// element argument can be a selector string
//   for an individual element
// var msnry = new Masonry( '.grid', {
//   // options
// });


// const container = document.querySelector('.gallery-box');
// const msnry;
// // Инициализация Масонри, после загрузки изображений
// imagesLoaded( container, function() {
// 	msnry = new Masonry( container );
// });