"use strict"

// Переключатель табов  в блоке Our services

let tabItems = document.querySelectorAll(".content-services-link");
let contentItem = document.querySelectorAll(".content-services-items");

tabItems.forEach(function(element) {
   element.addEventListener("click", function() {
        let currentDataTab = this.getAttribute("data-tab");
        let content = document.querySelector('.content-services-items[data-tab="'+currentDataTab+'"]')
        
        let activeTab = document.querySelector(".content-services-link.services-link-active");
        let activeContent = document.querySelector(".content-services-items.active-services");
        
        activeTab.classList.remove("services-link-active"); 
        element.classList.add("services-link-active"); 
        
        activeContent.classList.remove("active-services"); 
        content.classList.add("active-services"); 
   });
});


//Фильтр в блоке Our Amazing Work

const imageFilter = document.querySelectorAll(".content-amazing-work-image-item");
let filterClick = document.querySelector(".content-filter-amazing-work");

filterClick.addEventListener("click", function(evt){

    if(evt.target.tagName !== "LI") {
        return false;
    }
    let filterClass = evt.target.closest("a").dataset["filter"];

    console.log(evt.currentTarget);// 
    console.log(filterClass);//
    console.log(evt.target.closest("a"));

    imageFilter.forEach(function(element){
        element.classList.remove("imageHidden")
        if(!element.classList.contains(filterClass) && filterClass !== "all"){
            element.classList.add("imageHidden");
}
    })
})

//Подгрузка картинок в блоке Our Amazing Work

const btn = document.querySelector(".button-load-more");

btn.addEventListener("click", function (){

    let imageBox = document.querySelector(".content-amazing-work-image.loadImage");

    imageBox.classList.remove("loadImage");

})