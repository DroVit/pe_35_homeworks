"use strict"

let tabItems = document.querySelectorAll(".tab-item");
let contentItem = document.querySelectorAll(".content-item");

tabItems.forEach(function(element) {
   element.addEventListener("click", function() {
        let currentDataTab = this.getAttribute("data-tab");
        let content = document.querySelector('.content-item[data-tab="'+currentDataTab+'"]')
        
        let activeTab = document.querySelector(".tab-item.active");
        let activeContent = document.querySelector(".tab-content.content-item.active");
        
        activeTab.classList.remove("active"); 
        element.classList.add("active"); 
        
        activeContent.classList.remove("active"); 
        content.classList.add("active"); 
   });
});